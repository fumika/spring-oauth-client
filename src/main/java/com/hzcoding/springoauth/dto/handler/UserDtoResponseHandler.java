package com.hzcoding.springoauth.dto.handler;

import com.hzcoding.springoauth.dto.UserDto;
import com.hzcoding.springoauth.system.commons.httpclient.MkkHttpResponse;

/**
 * 15-5-18
 *
 * @author Shengzhao Li
 */
public class UserDtoResponseHandler extends AbstractResponseHandler<UserDto> {


    private UserDto userDto;

    public UserDtoResponseHandler() {
    }

    /*
    * Response is JSON or  XML (failed)
    *
    *  Error data:
    *  <oauth><error_description>Invalid access token: 3420d0e0-ed77-45e1-8370-2b55af0a62e8</error_description><error>invalid_token</error></oauth>
    *
    * */
    @Override
    public void handleResponse(MkkHttpResponse response) {
        if (response.isResponse200()) {
            this.userDto = responseToDto(response, new UserDto());
        } else {
            this.userDto = responseToErrorDto(response, new UserDto());
        }
    }


    public UserDto getUserDto() {
        return userDto;
    }


}
