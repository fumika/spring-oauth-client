package com.hzcoding.springoauth.service;

import com.hzcoding.springoauth.dto.AccessTokenDto;
import com.hzcoding.springoauth.dto.AuthAccessTokenDto;
import com.hzcoding.springoauth.dto.AuthCallbackDto;
import com.hzcoding.springoauth.dto.RefreshAccessTokenDto;
import com.hzcoding.springoauth.dto.UserDto;

/**
 * @author Shengzhao Li
 */

public interface OauthService {


    AccessTokenDto retrieveAccessTokenDto(AuthAccessTokenDto tokenDto);

    AuthAccessTokenDto createAuthAccessTokenDto(AuthCallbackDto callbackDto);

    UserDto loadUnityUserDto(String accessToken);

    AccessTokenDto retrievePasswordAccessTokenDto(AuthAccessTokenDto authAccessTokenDto);

    AccessTokenDto refreshAccessTokenDto(RefreshAccessTokenDto refreshAccessTokenDto);

    AccessTokenDto retrieveCredentialsAccessTokenDto(AuthAccessTokenDto authAccessTokenDto);
}