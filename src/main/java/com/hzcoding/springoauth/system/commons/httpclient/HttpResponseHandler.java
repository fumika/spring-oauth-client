package com.hzcoding.springoauth.system.commons.httpclient;

/**
 * @author Shengzhao Li
 */

public interface HttpResponseHandler {


    public void handleResponse(MkkHttpResponse response);

}