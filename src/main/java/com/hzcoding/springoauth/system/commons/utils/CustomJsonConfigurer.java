package com.hzcoding.springoauth.system.commons.utils;

import net.sf.json.JsonConfig;

/**
 * @author Shengzhao Li
 */

public interface CustomJsonConfigurer {


    void config(JsonConfig jsonConfig);

}